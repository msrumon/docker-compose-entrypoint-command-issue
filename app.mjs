import Fastify from 'fastify';

const fastify = Fastify({ logger: true });

fastify.get('/', async (_request, _reply) => {
  return { foo: 'bar' };
});

try {
  await fastify.listen({ port: 1234, host: '0.0.0.0' });
} catch (error) {
  fastify.log.error(err);
  process.exit(1);
}
